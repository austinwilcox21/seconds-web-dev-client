const gulp = require("gulp");
const autoprefixer = require("gulp-autoprefixer");
const cssnano = require("gulp-cssnano");
const rename = require("gulp-rename");
const notify = require("gulp-notify");
const sass = require("gulp-sass");
const imagemin = require("gulp-imagemin");

function minifyImages() {
  return gulp
    .src("public/Images/*")
    .pipe(
      imagemin(
        [
          imagemin.mozjpeg({ quality: 75, progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 })
        ],
        { verbose: true }
      )
    )
    .pipe(gulp.dest("public/Images/minified"));
}

function styles() {
  return gulp
    .src("src/assets/SCSS/main.scss")
    .pipe(sass())
    .pipe(gulp.dest("src/assets/CSS"))
    .pipe(autoprefixer())
    .pipe(rename({ suffix: ".min" }))
    .pipe(cssnano())
    .pipe(gulp.dest("src/assets/CSS"))
    .pipe(notify({ message: "Styles task complete" }));
}

gulp.task("styles", () => {
  return styles();
});

gulp.task("minify", () => {
  return minifyImages();
});

gulp.task("watch:styles", function() {
  gulp.watch("src/assets/SCSS/*/*.scss", function() {
    return styles();
  });
});
