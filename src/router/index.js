import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/blog",
    name: "Blog",
    component: () => import("../views/Blog.vue")
  },
  {
    path: "/blog/:blogPost",
    name: "Blog Post",
    component: () => import("../views/BlogPost.vue")
  },
  {
    path: "/projects",
    name: "Project",
    component: () => import("../views/Projects.vue")
  },
  {
    path: "/tools",
    name: "Usefull Tool",
    component: () => import("../views/UsefullTools.vue")
  },
  {
    path: "/resume",
    name: "Resume",
    component: () => import("../views/Resume.vue")
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
