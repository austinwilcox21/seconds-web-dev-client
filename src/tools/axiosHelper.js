const axios = require("axios");

const Axios = axios.create({
  baseURL: process.env.VUE_APP_BASEURL
});

module.exports = { Axios };
